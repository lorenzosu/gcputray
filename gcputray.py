#!/usr/bin/python
"""
Small pyGTK tray CPU setting app. Needs to be able to call cpufreq-set

Various ideas taken from Gtk tutorial at:
http://learngtk.org/pygtk-tutorial
"""
import subprocess
import os
import sys
import gtk
import glib
import cpuutils

# Set this to True to get debug messages
GLOBAL_DEBUG = True

def printdb (the_string):
    """ Quick and dirty debug print """
    if GLOBAL_DEBUG == True:
        print ('DBG: %s') % (the_string)

class AppData:
    """ Small class to hold applicaton data """
    def __init__(self):
        self.name = "gCPUtray"
        self.tip = ''
        self.refresh_seconds = 2
        self.version = "0.3"
        self.authors = ["Lorenzo Sutton"]
        self.website = "http://gitorious.org/gcputray"

class GCPUTray:
    """ Main class for the application """
    def __init__(self):
        # Create CPUInfo class
        self.cpu_utils = cpuutils.CPUUtils()
        printdb ("CPUS: %s" % self.cpu_utils.cpu_list)
        printdb ("Governors: %s" % self.cpu_utils.gov_list)
        printdb ("Current governor: %s" % self.cpu_utils.current_gov)
        self.appdata = AppData()
        #self.appdata.tip = self.cpu_utils.current_gov
        self.appdata.tip = self.cpu_utils.current_gov + self.cpu_utils.current_freq
        self.statusicon = gtk.StatusIcon()

        icon_path = os.path.join (sys.path[0], 'icons/')
        self.icons = {
                    'performance':'performance.png',
                    'ondemand':'ondemand.png',
                    'powersave':'powersave.png',
                    'conservative':'conservative.png',
                    'default':'icon.png'
                }
        for key in self.icons.keys():
            self.icons[key] = os.path.join(icon_path, self.icons[key])
        printdb(self.icons)
        self.refresh()
        self.statusicon.connect("popup-menu", self.right_click_event)
        self.statusicon.set_tooltip(self.appdata.tip)
        self.window = gtk.Window()
        self.window.connect("destroy", lambda w: gtk.main_quit())
        self.window.set_skip_taskbar_hint(True)
        self.window.set_icon(None)
        glib.timeout_add_seconds(self.appdata.refresh_seconds, self.refresh)

    def refresh(self):
        """ Callback that refreshes the current CPU governor and status icon """
        self.cpu_utils.get_current()
        self.cpu_utils.get_current_freq()
        self.appdata.tip = self.cpu_utils.current_gov + (" (%s)") % (self.cpu_utils.current_freq)
        self.statusicon.set_tooltip(self.appdata.tip)
        self.set_status_icon()
        return True

    def set_status_icon(self):
        """ Set the status icon depending on governor """
        icon_path = ''
        if self.cpu_utils.current_gov in self.icons.keys():
            icon_path = self.icons[self.cpu_utils.current_gov]
        else:
            icon_path = self.icons['default']
            
        self.icon_pixbuf = gtk.gdk.pixbuf_new_from_file(icon_path)
        self.statusicon.set_from_pixbuf(self.icon_pixbuf)
        self.icon_size = self.statusicon.get_size()
            
    def right_click_event(self, icon, button, menu_time):
        """ This is called when the icon is right-clicked """
        menu = gtk.Menu()
        for this_gov in self.cpu_utils.gov_list:
            menu_item = gtk.ImageMenuItem(this_gov)
            if this_gov == self.cpu_utils.current_gov: 
                # make the currently selected one bold
                lab = menu_item.get_children()[0]
                lab.set_markup ('<b>' + this_gov + '</b>')
            image = gtk.Image()
            try:
                image_file = self.icons[this_gov]
            except KeyError:
                image_file = self.icons['default']
            pixbuf = gtk.gdk.pixbuf_new_from_file(image_file)
            pixbuf = pixbuf.scale_simple(20, 20, gtk.gdk.INTERP_BILINEAR)
            image.set_from_pixbuf(pixbuf)
            menu_item.set_image(image)
            menu.append (menu_item)
            width = menu.get_allocation().width
            height = menu_item.get_allocation().height
            menu_item.connect("activate", self.set_governor, this_gov)
        menu_sep = gtk.SeparatorMenuItem()
        current_freq_item = gtk.MenuItem(self.cpu_utils.current_freq)

        about = gtk.ImageMenuItem("About")
        about_image = gtk.Image()
        about_image.set_from_stock(gtk.STOCK_ABOUT, gtk.ICON_SIZE_MENU)
        about.set_image(about_image)
        quit_menu = gtk.ImageMenuItem("Quit")
        quit_image = gtk.Image()
        quit_image.set_from_stock(gtk.STOCK_QUIT, gtk.ICON_SIZE_MENU)
        quit_menu.set_image(quit_image)
        about.connect("activate", self.show_about_dialog)
        quit_menu.connect("activate", gtk.main_quit)
        menu.append(menu_sep)
        menu.append(current_freq_item)
        menu.append(gtk.SeparatorMenuItem())
        menu.append(about)
        menu.append(quit_menu)
        menu.show_all()
        
        menu.popup(
            None,
            None,
            gtk.status_icon_position_menu,
            button,
            menu_time,
            self.statusicon
        )
        
    def show_about_dialog(self, widget):
        """ Show the about dialog """
        about_dialog = gtk.AboutDialog()
        about_dialog.set_destroy_with_parent(True)
        about_dialog.set_logo(self.icon_pixbuf)
        about_dialog.set_name(self.appdata.name)
        about_dialog.set_version(self.appdata.version)
        about_dialog.set_authors(self.appdata.authors)
        about_dialog.set_website(self.appdata.website)
        about_dialog.run()
        about_dialog.destroy()

    def set_governor (self, widget, governor):
        """ Run the cpupower frequenct-set command with governor """
        #TODO this requires sudo How to let it run as user
        printdb ("Selected: %s" % governor)
        if governor == self.cpu_utils.current_gov:
            # Selected the already current one
            printdb("%s is already the current" % governor)
            return -1
        errors = 0
        """ cpupower now does all CPUs automatically """
        try:
            """ Call the command """
            #TODO Is it possible to do this directly without using the command?
            out = subprocess.check_output (
                ['cpupower', 'frequency-set', '-g %s' % governor]
            )
            self.cpu_utils.get_current()
            self.appdata.tip = (
                    self.cpu_utils.current_gov + self.cpu_utils.current_freq
                    )
            self.statusicon.set_tooltip(self.appdata.tip)
            self.set_status_icon()
            printdb("\n" + out)
        except subprocess.CalledProcessError as err:
            errors += 1
            error_string = ((
                "Error calling cpupower frequency-set. Code %d.\nMessage: %s"
                ) %
                (err.returncode, err.output))
            self.error_message (error_string)
        except OSError as err:
            errors += 1
            error_string = (
                    (
                    "Error calling cpupower frequency-set.\nError: %s"
                    ) %
                    (err)
                )
            if err.errno == 2:
                error_string = (
                        error_string +
                        """
If cpupower is not present try installing e.g.:
Fedora: dnf install kernel-tools
                        """
                        )
            self.error_message (error_string)
        return errors
    
    def error_message (self, message_string):
        """ Simple error message function """
        messagedialog = gtk.MessageDialog(self.window,
                    0,
                    gtk.MESSAGE_ERROR,
                    gtk.BUTTONS_OK,
                    message_string)
        messagedialog.run()
        messagedialog.destroy()             

GCPUTray()
gtk.main()

import subprocess
import os

class CPUUtils:
    """ A class for getting info and managing CPU.
    """
    def __init__(self):
        self.maxcpu = 16
        self.cpu_base_path = "/sys/devices/system/cpu"
        self.path_cur_gov = "cpufreq/policy0/scaling_governor"
        self.path_avail_gov = "cpufreq/policy0/scaling_available_governors"
        self.current_gov = None
        self.current_freq = None
        self.freq_file = None
        self.cpu_list = []
        self.gov_list = None
        self.setup()
    
    def setup(self):
        """ Set-up function called when initiated """
        self.get_governors()
        self.count_cpus()
        self.get_current()

        # In latest kernels (4.14? and 5+) path for frequency seems to have changed...
        # Trying various ones: will check if any of these exist...
        possible_freq_files = ['cpuinfo_cur_freq', 'scaling_cur_freq']
        for this_file in possible_freq_files:
            current_path = os.path.join(
                    self.cpu_base_path,
                    "cpu0/cpufreq",
                    this_file
                    )
            if os.path.exists(current_path):
                self.freq_file = current_path
                break
        self.get_current_freq()

    def count_cpus(self):
        """ Count the number of CPUs by checking if the
        /sys/devices/system/cpu/cpu* directories exists up to self.maxcpu
        """
        cpu_base_path = self.cpu_base_path
        for i in range(0, self.maxcpu):
            this_cpu = os.path.join(cpu_base_path, "cpu" + str(i))
            if os.path.exists(this_cpu):
                self.cpu_list.append(i)
            else:
                return 0
        print self.cpu_list
    
    def get_governors(self):
        """ Extract available governors by reading the contents of the file
        in self.cpu_base_path and filename self.path_avail_gov
        Obviously this assumes that this information is the same for all CPUs
        """
        avail_gov_file = os.path.join(
                self.cpu_base_path,
                self.path_avail_gov
                )
        with open(avail_gov_file, 'r') as f:
            output_string = f.readline()
        output_string = output_string.strip()
        self.gov_list = output_string.split(' ')
        self.gov_list.sort()
        return 0
        
    def get_current(self):
        """ Get the current governor from file
        in self.cpu_base_path and filename self.path_cur_gov
        Again *this assumes all CPUs are set to the same governor!! """

        cur_gov_file = os.path.join(
                self.cpu_base_path,
                self.path_cur_gov
                )
        f = open(cur_gov_file, 'r')
        output_string = f.readline()
        output_string = output_string.strip()
        f.close()
        self.current_gov = output_string

    def get_current_freq(self):
        """ Get the current frequency. """
        
        freq_file = self.freq_file
        with open(freq_file, 'r') as f:
            freq_value = f.readline()
        freq_value = freq_value.strip()
        self.current_freq = ("%.2f G") % (float(freq_value) / 1000000.)
        return 0
